terraform {
  backend "s3" {
    key    = "terraform/gitlab-runner.tfstate"
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.64"
    }
    # set the envvar GITLAB_TOKEN to auto-authentication
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 15.11"
    }
    http = {
      source  = "hashicorp/http"
      version = "~> 3.2"
    }
  }
  required_version = ">=1.4"
}

provider "aws" {
  region = local.aws_region
  default_tags {
    tags = {
      Environment = terraform.workspace
      Owner       = "jlamande"
      Project     = "efrei-pipelines"
      ManagedBy   = "terraform"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}
