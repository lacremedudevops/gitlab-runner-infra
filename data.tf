data "gitlab_group" "infra" {
  full_path = var.gitlab_group_path
}

data "aws_ami" "amazon_linux_2023" {
  most_recent = true
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
  filter {
    name   = "name"
    values = ["al2023-ami*arm64*"]
  }
}

########################
# Find your IP
########################
data "http" "current_ip" {
  url = "https://checkip.amazonaws.com"
}
