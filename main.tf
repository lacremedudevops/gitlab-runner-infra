locals {
  aws_region = "us-east-1"
  namespace  = "gitlab-infra"

  vpc_cidr             = "172.16.0.0/16"
  public_subnet_cidr   = "172.16.1.0/24"
  current_ip_address   = chomp(data.http.current_ip.response_body)
  allowed_ip_addresses = distinct(concat(var.personal_ip_addresses, [local.current_ip_address]))

  gitlab_runner_image        = "public.ecr.aws/gitlab/gitlab-runner:latest"
  gitlab_runner_token        = data.gitlab_group.infra.runners_token
  gitlab_runner_token_in_ssm = "/gitlab_runner/registration_token"
}

# Create the VPC
resource "aws_vpc" "infra" {
  cidr_block = local.vpc_cidr
  tags = {
    Name = local.namespace
  }
}

# Create the public subnet
# tfsec:ignore:aws-ec2-no-public-ip-subnet internet exposure
resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.infra.id
  cidr_block              = local.public_subnet_cidr
  availability_zone       = "${local.aws_region}a"
  map_public_ip_on_launch = true
  tags = {
    Name = "${local.namespace}-public"
  }
}

# Create an internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.infra.id
  tags = {
    Name = local.namespace
  }
}

# Create a route table for the public subnets
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.infra.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "${local.namespace}-public"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

# Gitlab Runner EC2 access
# tfsec:ignore:aws-ec2-no-public-egress-sgr internet exposure
resource "aws_security_group" "gitlab_runner_sg" {
  name_prefix = "${local.namespace}-gitlab-runner"
  vpc_id      = aws_vpc.infra.id
  # ssh ingress allowed
  # but using SSM sessions is a more secure way for remote shell access and does not require ingress unlike ssh.
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [for ip in local.allowed_ip_addresses : "${ip}/32"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [local.public_subnet_cidr]
  }
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [local.public_subnet_cidr]
  }
  tags = {
    "Name" = "${local.namespace}-gitlab-runner"
  }
}

# saves securely the gitlab registration token
resource "aws_ssm_parameter" "gitlab_runner_token" {
  name  = local.gitlab_runner_token_in_ssm
  type  = "SecureString"
  value = local.gitlab_runner_token
}

# Create an instance EC2 for running Docker Gitlab Executor
# tfsec:ignore:aws-ec2-enforce-http-token-imds simplify retrieval of creds for AWS in the runner
resource "aws_instance" "gitlab_runner" {
  ami                  = data.aws_ami.amazon_linux_2023.id
  key_name             = "vockey"
  iam_instance_profile = "LabInstanceProfile"
  instance_type        = var.instance_type
  lifecycle {
    create_before_destroy = true
  }
  # metadata_options {
  #   http_endpoint               = "enabled"
  #   http_tokens                 = "required"
  #   http_put_response_hop_limit = 1
  # }
  root_block_device {
    encrypted = true
  }
  subnet_id = aws_subnet.public.id
  tags = {
    Name = "gitlab-runner"
  }

  # EC2 start script
  user_data                   = <<-EOF
    #!/bin/bash
    dnf update -y
    dnf install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_arm64/amazon-ssm-agent.rpm
    systemctl status amazon-ssm-agent
    dnf install -y docker
    usermod -a -G docker ec2-user
    systemctl enable docker
    systemctl start docker

    REGISTRATION_TOKEN=$(aws ssm get-parameter --name ${local.gitlab_runner_token_in_ssm} --region ${local.aws_region} --with-decryption --query 'Parameter.Value' --output text)

    # Register the runner with GitLab
    # https://docs.gitlab.com/runner/register/index.html#one-line-registration-command
    docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner "${local.gitlab_runner_image}" register \
        --non-interactive \
        --executor "docker" \
        --description "docker-runner" \
        --docker-image alpine:latest \
        --url "https://gitlab.com/" \
        --registration-token "$REGISTRATION_TOKEN" \
        --run-untagged="true" \
        --tag-list "docker,aws,terraform"
    
    sed -i -e 's/^concurrent.*/concurrent=10/g' /srv/gitlab-runner/config/config.toml

    # start the gitlab-runner container
    docker run -d --name gitlab-runner --restart always \
        -v /srv/gitlab-runner/config:/etc/gitlab-runner \
        -v /var/run/docker.sock:/var/run/docker.sock \
        ${local.gitlab_runner_image}
    EOF
  user_data_replace_on_change = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_sg.id]
}
