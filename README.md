# Gitlab CI Runner on Lab

[TOC]

## Overview

![AWS Architecture](./gitlab-runner-lab.png)

## Deploy the Gitlab CI Runner

1. Create an S3 Bucket (choose a unique name for your bucket) :
    - using the AWS Web Console https://s3.console.aws.amazon.com/s3/home?region=us-east-1#
    - or using the Lab Terminal : `aws s3api create-bucket --bucket efrei-pipelines-samples-infra`
2. On [Gitlab.com](https://gitlab.com)
    1. Create a personal access **with scope `api`** : <https://gitlab.com/-/profile/personal_access_tokens>
    2. Create a gitlab private group for storing your future work (sources, pipelines) or use one you already created
    <!-- 3. create 2 gitlab CI/CD variables (go to Settings > CI/CD and expand the "Variables" section)
        -  Key: `TF_BUCKET`
           Value: the name of your bucket
        - Key: `TF_PLAN_ENCRYPTION_KEY`
          Value: any value (ong and random is better)
          Masked (ensure option `Mask variable` is selected) -->
3. In the Learner Lab Terminal
    1. Install [Terraform](https://www.terraform.io/)
        ```sh 
        wget https://releases.hashicorp.com/terraform/1.7.1/terraform_1.7.1_linux_amd64.zip
        mkdir -p ~/terraform
        unzip -o terraform_1.7.1_linux_amd64.zip -d ~/terraform
        export PATH="$PATH:$HOME/terraform"
        terraform version
        ```
    2. Clone the repository : `git clone https://gitlab.com/lacremedudevops/gitlab-runner-infra`
    3. Execute terraform to deploy the gitlab runner
        ```sh
        cd gitlab-runner-infra

        terraform init
        # or (useful for pipelines)
        # terraform init -backend-config 'bucket=efrei-pipelines-samples-infra'

        terraform plan -out tf.plan
        # or (with your own gitlab group)
        # terraform plan -var="gitlab_group_path=efrei-devops/pipelines/samples" -var="gitlab_token=glpat-xxxxxxxxxxxx" -out tf.plan

        terraform apply tf.plan
        ```

On your gitlab group, go to "CICD / Runners" and check that a runner becomes available and "online" : <https://gitlab.com/groups/efrei-devops/pipelines/samples/-/runners>. It may take a few minutes before the runner appears.

In case of troubles, check the terraform logs, go to the AWS Console and check for the [EC2 Instances](https://us-east-1.console.aws.amazon.com/ec2/home?region=us-east-1#Instances:) : an instance named `gitlab-runner` should be running.

Now when you define pipelines (with a file `.gitlab-ci.yml`) in any repository of your gitlab group they will automatically use this runner.
<!-- 
### Run the samples

Copy the samples :

- Fork or copy these repositories to your gitlab group :
    - <https://gitlab.com/efrei-devops/pipelines/samples/build-ami> (fork link : <https://gitlab.com/efrei-devops/pipelines/samples/build-ami/-/forks/new>)
    - <https://gitlab.com/efrei-devops/pipelines/samples/webapp-ha> (fork link : <https://gitlab.com/efrei-devops/pipelines/samples/webapp-ha/-/forks/new>)

**For automatic chaining of the 2 pipelines, fork them in the same gitlab group and don't change the name of `webapp-ha` project.**

Execute the samples pipelines :

- Run the `build-ami` pipeline (go to CI/CD and execute "Run Pipeline")
    - the build takes a few minutes
    - then it automatically triggers a second pipeline for deployment of the AMI

Explore the pipelines :

- the stages
- the jobs
- jobs logs, artifacts
- available actions/variables/... in the pipelines
- the sources
- ...

In case of troubles, check the jobs logs, check that you have correcty created the 2 CI/CD Variables (`TF_BUCKET` and `TF_PLAN_ENCRYPTION_KEY`). -->

### Next Steps

- Create your own repositories in your gitlab group
- Define your own pipelines using the Gitlab CI yml reference : https://docs.gitlab.com/ee/ci/yaml/
<!-- 
Some ideas :

- Choose another application (one you developed or an opensource one) a bit more powerful (using database, ...)
- Create a production workflow : deploy to a dev environment/infrastructure then to a production/live environment
- Split base infrastructure deployment (VPC, network, ...) and application deployment
- Securize access to the application using HTTPS and free certificates as [Let's Encrypt](https://letsencrypt.org/fr/) and AWS Certificate Manager (example but need to be automated : <https://itnext.io/using-letsencrypt-ssl-certificates-in-aws-certificate-manager-c2bc3c6ae10>)
- Add some quality controls of code
    - [terraform format check](https://developer.hashicorp.com/terraform/language/functions/format)
    - security with [tfsec](https://github.com/aquasecurity/tfsec) or [checkov](https://www.checkov.io/)
- Add some automated tests
- Publish reports as [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- Build and deploy your application as a Docker Container in a Kubernetes cluster (EKS on amazon) instead of an AMI on EC2
- Use Terraform Cloud for advanced management (state, ...)
- Switch to another CICD System (Github Actions, ...) -->

## Cleaning up
<!-- 
### Samples

_to do before the cleanup of the gitlab runner infrastructure_

In the last successful `webapp-ha` pipeline :

- run the `destroy` job

In the last successful `build-ami` pipeline : 

- run the `destroy all APP_NAME images` job -->


### Gitlab runner infrastructure

In the Lab Terminal :

```sh
cd gitlab-runner-infra
terraform destroy

# s3 cleanup (replace with your bucket name)
aws s3 rm s3://efrei-pipelines-samples-infra/terraform/gitlab-runner.tfstate
aws s3 rb s3://efrei-pipelines-samples-infra
```

## Terraform technical documentation

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
### Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.4 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.64 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | ~> 15.11 |
| <a name="requirement_http"></a> [http](#requirement\_http) | ~> 3.2 |

### Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 4.64 |
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | ~> 15.11 |
| <a name="provider_http"></a> [http](#provider\_http) | ~> 3.2 |

### Modules

No modules.

### Resources

| Name | Type |
|------|------|
| [aws_instance.gitlab_runner](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_internet_gateway.igw](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_route_table.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_security_group.gitlab_runner_sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ssm_parameter.gitlab_runner_token](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_subnet.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.infra](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_ami.amazon_linux_2023](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [gitlab_group.infra](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |
| [http_http.current_ip](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http) | data source |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gitlab_group_path"></a> [gitlab\_group\_path](#input\_gitlab\_group\_path) | Path to your Gitlab Group | `string` | n/a | yes |
| <a name="input_gitlab_token"></a> [gitlab\_token](#input\_gitlab\_token) | A Gitlab Personal Access token | `string` | n/a | yes |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | EC2 Instance type of the runner | `string` | `"t4g.micro"` | no |
| <a name="input_personal_ip_addresses"></a> [personal\_ip\_addresses](#input\_personal\_ip\_addresses) | Your personal IP addresses to allow access from | `list(string)` | `[]` | no |

### Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
