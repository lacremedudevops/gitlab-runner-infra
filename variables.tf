variable "gitlab_token" {
  description = "A Gitlab Personal Access token"
  sensitive   = true
  type        = string
}

variable "gitlab_group_path" {
  description = "Path to your Gitlab Group"
  type        = string
}

variable "instance_type" {
  description = "EC2 Instance type of the runner"
  default     = "t4g.medium"
  type        = string
}

# Change with your IPs
# or pass the variable to the terraform command :
# terraform plan -var='personal_ip_addresses=["ip1","ip2"]'
# https://developer.hashicorp.com/terraform/language/values/variables#variables-on-the-command-line
variable "personal_ip_addresses" {
  description = "Your personal IP addresses to allow access from"
  default     = []
  type        = list(string)
}
